package ly.bithive.space_layout;


import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

public class SpaceTabLayoutBehavior extends CoordinatorLayout.Behavior<SpaceTabLayout> {

    public SpaceTabLayoutBehavior() {
        super();
    }

    public SpaceTabLayoutBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, SpaceTabLayout child, View dependency) {
        float translationY = Math.min(0, dependency.getTranslationY() - dependency.getHeight());
        child.setTranslationY(translationY);
        return true;
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, SpaceTabLayout child, View dependency) {
        boolean dependsOn = dependency instanceof FrameLayout;
        return dependsOn;
    }

    @Override
    public boolean onStartNestedScroll(CoordinatorLayout coordinatorLayout, SpaceTabLayout child, View directTargetChild, View target, int nestedScrollAxes) {
        return nestedScrollAxes == ViewCompat.SCROLL_AXIS_VERTICAL;
    }

    @Override
    public void onNestedPreScroll(CoordinatorLayout coordinatorLayout, SpaceTabLayout child, View target, int dx, int dy, int[] consumed) {
        if (dy < 0) {
            showSpaceTabLayout(child);
        } else if (dy > 0) {
            hideSpaceTabLayout(child);
        }
    }

    private void hideSpaceTabLayout(SpaceTabLayout view) {
        view.animate().translationY(view.getHeight());
    }

    private void showSpaceTabLayout(SpaceTabLayout view) {
        view.animate().translationY(0);
    }
}
